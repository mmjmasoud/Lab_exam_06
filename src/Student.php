<?php

namespace App;

class Student
{
    public $id = '',
            $fullName = '',
            $courseName= '',
            $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'labxm6b22');
    }

    public function prepare($data = [])
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('fullName', $data)) {
            $this->fullName = $data['fullName'];
        }
        if(array_key_exists('courseName', $data)) {
            $this->courseName = $data['courseName'];
        }

        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO `labxm6b22`.`student_information` (`fullName`, `courseName`) VALUES ('". $this->fullName ."', '".$this->courseName."')";

        if(mysqli_query($this->conn, $query)) {
            Message::message('Data Successfully deleted');
            header("Location: index.php");
        }

    }

    public function index()
    {
        $allStudents = [];
        $query = "SELECT * FROM `student_information` WHERE `deleted_at` IS NULL";

        $result = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($result)) {
            $allStudents[] = $row;
        }

        return $allStudents;
    }

    public function trashedIndex()
    {
        $allStudents = [];
        $query = "SELECT * FROM `student_information` WHERE `deleted_at` IS NOT NULL";

        $result = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($result)) {
            $allStudents[] = $row;
        }

        return $allStudents;
    }

    public function delete()
    {
        $query = "DELETE FROM `labxm6b22`.`student_information` WHERE `id` =" .$this->id;
        if(mysqli_query($this->conn, $query)) {
            Message::message('Data Successfully deleted');
            header("Location: index.php");
        }
    }

    public function view()
    {
        $query = "SELECT * FROM `student_information` WHERE `id`= " .$this->id;

        $result = mysqli_query($this->conn, $query);

        $row = mysqli_fetch_assoc($result);

        return $row;
    }

    public function update()
    {
        $query = "UPDATE `labxm6b22`.`student_information` SET `fullName` = '".$this->fullName."', `courseName` = '".$this->courseName."' WHERE `id` = ". $this->id;

        if(mysqli_query($this->conn, $query)) {
            Message::message('Data Successfully Updated');
            header("Location: index.php");
        }
    }
    public function trash()
    {
        $time = time();
        $query = "UPDATE `labxm6b22`.`student_information` SET `deleted_at` = '{$time}' WHERE `id` = ". $this->id;

        if(mysqli_query($this->conn, $query)) {
            Message::message('Data Successfully Trashed');
            header("Location: index.php");
        }
    }

    public function recover()
    {
        $query = "UPDATE `labxm6b22`.`student_information` SET `deleted_at` = NULL WHERE `id` = ". $this->id;

        if(mysqli_query($this->conn, $query)) {
            Message::message('Data Successfully Recovered');
            header("Location: index.php");
        }
    }

    public function deleteMultiple($ids)
    {
        $query = "DELETE FROM `labxm6b22`.`student_information` WHERE `id` IN ({$ids})";
        if(mysqli_query($this->conn, $query)) {
            Message::message('All Data Successfully deleted');
            header("Location: index.php");
        }
    }

    public function recoverMultiple($ids)
    {
        $query = "UPDATE `labxm6b22`.`student_information` SET `deleted_at` = NULL WHERE `id` IN ({$ids})";
        if(mysqli_query($this->conn, $query)) {
            Message::message('All Data Successfully Recovered');
            header("Location: index.php");
        }
    }
}