<?php


namespace App;


if(!isset($_SESSION['message'])) {
    session_start();
}

class Message
{
    public static function message( $message = null )
    {
        if($message == null) {
            return self::getMessage();
        } else {
            self::setMessage($message);
        }
    }

    public static function getMessage()
    {
        $message = $_SESSION['message'];

        $_SESSION['message'] = '';

        return $message;
    }

    public static function setMessage($message)
    {
        $_SESSION['message'] = $message;
    }
}