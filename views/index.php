<?php
session_start();
require_once '../vendor/autoload.php';

use App\Student;
use App\Message;

$student = new Student();

$students = $student->index();

if(isset($_SESSION['message'])) {
    $message = Message::message();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <h1>Students</h1>

            <p>
                <a href="create.php" class="btn btn-primary">Create</a>
                <a href="trashedall.php" class="btn btn-primary">All Trashed Items</a>
            </p>
            <?php if(!empty($message)): ?>
            <div class="alert alert-info">
                <?= $message ?>
            </div>
            <?php endif ?>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Courses</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <?php
                $sl = 0;
                foreach($students as $student):
                    $sl++;
                    ?>
                <tbody>
                    <tr>
                        <td><?= $sl ?></td>
                        <td><?= $student['id'] ?></td>
                        <td><?= $student['fullName'] ?></td>
                        <td><?= $student['courseName'] ?></td>
                        <td>
                            <a href="view.php?id=<?= $student['id'] ?>" class="btn btn-primary">View</a>
                            <a href="edit.php?id=<?= $student['id'] ?>" class="btn btn-primary">Edit</a>
                            <a href="delete.php?id=<?= $student['id'] ?>" class="btn btn-danger">Delete</a>
                            <a href="trash.php?id=<?= $student['id'] ?>" class="btn btn-danger">Trash</a>
                        </td>
                    </tr>
                </tbody>
                <?php endforeach ?>
            </table>

        </div>
    </div>
</div>

</body>
</html>