<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <p>
                <a href="index.php" class="btn btn-primary">HOME</a>
            </p>

            <h1>Create</h1>
            <form action="store.php" method="post">
                <div class="input-group">
                    <label for="name">Name</label>
                    <input type="text" name="fullName" id="name" class="form-control">
                </div>

                <h3>Select your courses:</h3>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="PHP"> PHP</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="Java"> Java</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="phython"> phython</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="dotnet"> dotnet</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="oracle"> oracle</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="html"> html</label>
                </div>
                <div class="input-group">
                    <input type="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>