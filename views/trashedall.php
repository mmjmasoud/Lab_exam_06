<?php
session_start();
require_once '../vendor/autoload.php';

use App\Student;
$student = new Student();

$students = $student->trashedIndex();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <p>
                <a href="index.php" class="btn btn-primary">HOME</a>
            </p>

            <h1>Students</h1>


            <?php if(!empty($message)): ?>
                <div class="alert alert-info">
                    <?= $message ?>
                </div>
            <?php endif ?>
            <form action="deletemultiple.php" method="post" id="tForm">
                <p>
                    <a href="create.php" class="btn btn-primary">Create</a>
                    <button type="submit"class="btn btn-success" id="rAll">Recover All</button>
                    <button type="submit"class="btn btn-danger">Delete All</button>
                </p>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Check</th>
                        <th>SL</th>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Courses</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <?php
                    $sl = 0;
                    foreach($students as $student):
                        $sl++;
                        ?>
                        <tbody>
                        <tr>
                            <td>
                                <input type="checkbox" name="chk[]" value="<?= $student['id'] ?>">
                            </td>
                            <td><?= $sl ?></td>
                            <td><?= $student['id'] ?></td>
                            <td><?= $student['fullName'] ?></td>
                            <td><?= $student['courseName'] ?></td>
                            <td>
                                <a href="view.php?id=<?= $student['id'] ?>" class="btn btn-primary">View</a>
                                <a href="recover.php?id=<?= $student['id'] ?>" class="btn btn-primary">Recover</a>
                                <a href="delete.php?id=<?= $student['id'] ?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        </tbody>
                    <?php endforeach ?>
                </table>
            </form>

        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>
    $('#rAll').on('click', function() {
        $('#tForm').attr('action', 'recovermultiple.php').submit();
    })
</script>
</body>
</html>