<?php

require_once '../vendor/autoload.php';
use App\Student;

$student = new Student();

$students = $student->prepare($_GET)->view();

$courses = explode(', ', $students['courseName']);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <p>
                <a href="index.php" class="btn btn-primary">HOME</a>
            </p>

            <h1>Create</h1>
            <form action="update.php" method="post">
                <input type="hidden" name="id" value="<?= $students['id'] ?>">
                <div class="input-group">
                    <label for="name">Name</label>
                    <input type="text" name="fullName" id="name" class="form-control" value="<?= $students['fullName'] ?>">
                </div>

                <h3>Select your courses:</h3>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="PHP" <?if(in_array('PHP',$courses)) echo "checked"?>> PHP</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="Java" <?if(in_array('Java',$courses)) echo "checked"?>> Java</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="phython" <?if(in_array('phython',$courses)) echo "checked"?>> phython</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="dotnet" <?if(in_array('dotnet',$courses)) echo "checked"?>> dotnet</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="oracle" <?if(in_array('oracle',$courses)) echo "checked"?>> oracle</label>
                </div>
                <div class="input-group">
                    <label><input type="checkbox" name="std[]" value="html" <?if(in_array('html',$courses)) echo "checked"?>> html</label>
                </div>
                <div class="input-group">
                    <input type="submit" value="Update" class="btn btn-success">
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
