<?php

require_once '../vendor/autoload.php';
use App\Student;

$student = new Student();

$students = $student->prepare($_GET)->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <h1>Student</h1>

            <div class="list-group">
                <div class="list-group-item">ID: <?= $students['id'] ?></div>
                <div class="list-group-item">Full Name: <?= $students['fullName'] ?></div>
                <div class="list-group-item">Full Name: <?= $students['courseName'] ?></div>
            </div>

            <p>
                <a href="edit.php?id=<?= $students['id'] ?>" class="btn btn-primary">Edit</a>
                <a href="delete.php?id=<?= $students['id'] ?>" class="btn btn-danger">Delete</a>
            </p>

        </div>
    </div>
</div>

</body>
</html>